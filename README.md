# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

Basic:
(1)Basic control tools (30%)
Brush and eraser:icon是鉛筆與橡皮擦。按下鉛筆後可以搭配其他功能使用，並且會將畫布上的游標轉換成鉛筆，可以使用調色盤換顏色，或使用放大縮小改變其筆刷大小。橡皮擦按下下去之後，可以將畫布上的東西擦除，並無顏色，但可以隨著放大縮小按鍵改變橡皮擦的大小，按下後會出現橡皮擦的游標圖案。
Color selector:icon是一個位於最左邊的色票，點擊後可以選擇不同顏色轉換，而色票塊會依據不同顏色顯示不同色彩及其色碼。
Simple menu (brush size):網頁的上方為我的工具清單，而筆刷大小調整的icon則是一個像是放大鏡的綠色"+"與橘色"-"，每按一下"+"筆刷便會放大，按一下"-"筆刷則會縮小，一直按就會越來越大/小，這個調整大小的功能也適用於橡皮擦的大小及圓形、長方形、三角形等幾何圖案的邊框大小。

(2)Text input (10%)
User can type texts on canvas:icon是一個畫有T圖案的圖示。按下後再到畫布上點擊即可輸入文字並印在畫布上。輸入的文字會隨著顏色的調整顯示不同色彩，也會隨著文字大小的改變放大縮小，而還在文字框的狀態時字體就已經會隨著指定顏色呈現色彩，因此可以在輸入的過程中決定當前色彩是否適合該段文字及是否要切換其他色彩。
Font menu (typeface and size):typeface的icon是兩個下選單，左邊的是可以選擇文字特效，包含正常、粗體、斜體、小標字，而右邊的選單則可以選擇不同字型，而這裡有六種字體可以切換。size調整的icon則是畫著T+及T-的圖案，每按下T+便可以讓文字放大一些，而T-則是可以讓文字縮小一點，連按就會越大/越小。

(3)Cursor icon (10%)
The image should change according to the currently used tool:有自己主題游標功能的有鉛筆、橡皮擦、彩虹、筆刷自動縮放這四種。而鉛筆會顯示鉛筆的游標，橡皮擦則是橡皮擦，彩虹會出現一個可愛的雲中虹，至於筆刷自動縮放則是十字架的游標。這四種按鍵以外功能的游標則會承襲上述四種游標的icon來切換。

(4)Refresh button (10%)
Reset canvas:icon是兩個彎彎的箭頭互追的圖案。按下後整個畫布都會被清空成空白，但會保留原本設定的值，比如顏色、大小、常駐技能等等。

Advanced:
(1)Different brush shapes (15%)
Circle, rectangle and triangle (5% for each shape):其icon分別是圓圈、長方形、三角形，按下後即可在畫布上畫出圓圈或長方形或三角形的幾何圖案。

(2)Un/Re-do button (10%):undo icon是向左的藍色箭頭，而redo的icon則是向右的藍色箭頭。當undo被按下後，可以取消當前畫布的一次指令，且可以undo回最原始的畫布樣貌，而redo則可以一直回復到最新操作的該次畫布。

(3)Image tool (5%)
User can upload image and paste it:icon是一個有往上箭頭的資料夾圖案，按下這個按鍵後可以從本機端選擇一張圖片放到畫布中編輯。

(4)Download (5%)
Download current canvas as an image file:icon是一個有箭頭向下的風景照圖案，也是位於最右邊的按鈕。當其被按下後可以將當前的畫布儲存到本機端。

Other useful widgets:
(1)彩虹:icon是一個紅橘綠組成的水滴圖案，當按下後畫布上的游標會變成雲中虹，而且鉛筆及圓圈、長方形、三角形都可以跟著彩虹顏色變化，換句話說一個連續畫的鉛筆會有漸變的色彩軌跡。而這個是一個常駐技能，一開始為關閉，首次按下後則開啟，若欲取消則需再次點擊，可搭配筆刷自動縮放同時使用的特效。

(2)筆刷自動縮放:icon是一個筆刷畫出m圖案的圖示，當按下後畫布上的游標則會變成一個十字架，而筆刷會隨著軌跡的連續呈現一下放大一下縮小的變化。而這個也是一個常駐技能，一開始為關閉，首次按下後則開啟，若欲取消則需再次點擊，可搭配彩虹同時使用的特效。

(3)文字特效:icon是左邊的下拉選單，在這個選單中提供四種文字特效，分別是正常、粗體、斜體、小標字，以豐富文字輸入的變化性與多樣性。

(4)透明畫布及背景與虛線邊界:網頁上的美術特效，為活潑整個頁面，但又不想被實心材質顯得呆板，因此使用透明度的調整來增添畫面的和諧與美觀。

參考資料(有些遺漏沒有記錄到):
un/redo: http://www.htmleaf.com/ziliaoku/qianduanjiaocheng/201502151385.html
upload: https://stackoverflow.com/questions/10906734/how-to-upload-image-into-html5-canvas
download: https://www.sanwebe.com/snippet/downloading-canvas-as-image-dataurl-on-button-click
option: http://robertsong.pixnet.net/blog/post/336117808-%5B%E7%A8%8B%E5%BC%8F%5D%5Bjs%5D%5Bhtml%5D-%E8%A8%AD%E5%AE%9A-%E5%8F%96%E5%BE%97-%E4%B8%8B%E6%8B%89%E9%81%B8%E5%96%AE%28select%29%E9%81%B8%E5%8F%96
half-transparent: http://boohover.pixnet.net/blog/post/20556952-%E5%8D%8A%E9%80%8F%E6%98%8E%E8%88%87%E9%80%8F%E6%98%8E%E6%95%88%E6%9E%9C%E8%A8%AD%E8%A8%88-css-rgba%2C-hsla-%E8%89%B2%E5%BD%A9%E5%96%AE%E4%BD%8D-%28
drawer1: https://ithelp.ithome.com.tw/articles/10198838
drawer2: https://ithelp.ithome.com.tw/articles/10194162?sc=iThelpR
drawer3: https://codepen.io/hossman/pen/AyaFl
drawer4: https://wcc723.github.io/canvas/2014/12/09/html5-canvas-03/
text1: http://cssdeck.com/labs/g7uyao9z/
text2: https://www.w3schools.com/graphics/canvas_text.asp
CSS1: https://www.w3schools.com/css/css_dimension.asp
CSS2: http://www.wibibi.com/info.php?tid=79
CSS3: http://www.wibibi.com/info.php?tid=156
CSS flex: https://wcc723.github.io/css/2017/07/21/css-flex/
icon: https://www.flaticon.com/home
cursor: https://www.w3schools.com/jsref/prop_style_cursor.asp
color: http://jscolor.com/