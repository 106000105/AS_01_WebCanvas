/*取得canvas元素及渲染環境*/
var canvas=document.getElementById('myCanvas');
var ctx=canvas.getContext('2d');  //取得canvas的渲染環境及其繪圖函數

/*設定canvas寬高滿版*/
/*canvas.width=window.innerWidth;
canvas.height=window.innerHeight;*/
//console.log(canvas.width, canvas.height);

/*/初始設定*/
ctx.strokeStyle='#000000';  //筆觸顏色
ctx.lineJoin='round';   //兩條線交會處產生圓形邊角
ctx.lineCap='round';    //筆觸預設為圓形
ctx.lineWidth=5;    //筆頭寬度
ctx.font="normal 10px Arial";
canvas.style.cursor="url('pencil.png'), default";

var cPushArray = new Array();
var cStep = -1;
cPush();

let isDrawing=false;    //是否允許繪製(是否是mousedown下筆狀態)
var isRainbow=-1;
let hue=0;
var isDirection=-1;
let direction=true;

var nowTextsize=10;
var isText=true;
var FontEffect="normal";
var FontType="Arial";

let lastX=0;    //繪製時的起點座標
let lastY=0;    //繪製時的起點座標

var mousedown_X=0;
var mousedown_Y=0;

var nowColor='#000000';
var nowTool=0;
var canvasPic = new Image();

/*Reset*/
document.getElementById('reset').addEventListener('click', function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }, false);

/*繪圖函數撰寫1*/
/*========== Events Binding ==========*/
canvas.addEventListener('mouseup', function(){
    cPush();
    isDrawing = false;
    isText=true;
});
canvas.addEventListener('mouseout', () => isDrawing = false);
canvas.addEventListener('mousedown', (e) => {
    isDrawing = true;   //允許繪製
    [lastX, lastY]=[e.offsetX, e.offsetY];  //設定起始點
    [mousedownX, mousedownY]=[e.offsetX, e.offsetY];
});

canvas.addEventListener('mousemove', draw);

var imageLoader = document.getElementById('upload');
    imageLoader.addEventListener('change', upload, false);

/*========== 繪製函數；在 mousemove 的時候使用 ==========*/
function draw(e)
{
    //console.log(nowTool);
    switch(nowTool)
    {
        case 0: //pencil
            if(!isDrawing)  return; //沒有允許繪製即退出

            /*繪製路線 Setting*/
            if(isRainbow==1){
                ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色

                hue++;  //色相環度數更新
                if(hue>=360)
                {
                    hue=0;
                }
            }
            else if(isRainbow==-1){
                ctx.strokeStyle=nowColor;
            }
            if(isDirection==1){
                if(ctx.lineWidth>=100 || ctx.lineWidth<=1)
                {
                    direction=!direction;
                }

                //筆觸粗細實作
                if(direction)
                {
                    ctx.lineWidth++;
                }
                else
                {
                    ctx.lineWidth--;
                }
            }
            else if(isDirection==-1){
                ctx.lineWidth=ctx.lineWidth;
            }
            ctx.beginPath();    //開始路徑 or Reset
            ctx.moveTo(lastX, lastY);   //設定起點
            ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();
            //console.log(e.offsetX, e.offsetY);

            [lastX, lastY]=[e.offsetX, e.offsetY];  //位置更新

            break;

        case 1: //eraser
            if(!isDrawing)  return; //沒有允許繪製即退出

            /*繪製路線 Setting*/
            //ctx.strokeStyle='#FFFFFF';
            ctx.beginPath();    //開始路徑 or Reset
            ctx.moveTo(lastX, lastY);   //設定起點
            //ctx.lineTo(e.offsetX, e.offsetY);
            ctx.stroke();
            ctx.closePath();
            ctx.clearRect(lastX, lastY, ctx.lineWidth, ctx.lineWidth);

            [lastX, lastY]=[e.offsetX, e.offsetY];  //位置更新

            break;

        case 2: //text
            if(!isDrawing)  return; //沒有允許繪製即退出
            console.log(nowTool);
            /*繪製路線 Setting*/
            //ctx.font=FontEffect.toString()+" "+nowTextsize.toString()+"px"+" "+FontType.toString();
            ctx.beginPath();    //開始路徑 or Reset
            //ctx.moveTo(lastX, lastY);   //設定起點
            if(isText==true){
                isText=false;
                Context(e);
                //console.log(ctx.font);
            }

            break;

        case 3: //circle
            if(!isDrawing)  return; //沒有允許繪製即退出
            //console.log("rec");
            /*繪製路線 Setting*/
            if(isRainbow==1){
                ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色

                hue++;  //色相環度數更新
                if(hue>=360)
                {
                    hue=0;
                }
            }
            else if(isRainbow==-1){
                ctx.strokeStyle=nowColor;
            }
            canvasPic.src = cPushArray[cStep];            
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
            }
            ctx.beginPath();    //開始路徑 or Reset
            //ctx.moveTo(lastX, lastY);   //設定起點
            ctx.arc(lastX, lastY, e.offsetX-lastX, 0, 360, false);
            setTimeout(()=>{ctx.stroke()},5);
            ctx.closePath();

            break;

        case 4: //triangle
            if(!isDrawing)  return; //沒有允許繪製即退出
            //console.log("rec");
            /*繪製路線 Setting*/
            if(isRainbow==1){
                ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色

                hue++;  //色相環度數更新
                if(hue>=360)
                {
                    hue=0;
                }
            }
            else if(isRainbow==-1){
                ctx.strokeStyle=nowColor;
            }
            canvasPic.src = cPushArray[cStep];            
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
            }
            ctx.beginPath();    //開始路徑 or Reset
            ctx.moveTo(lastX, lastY);   //設定起點
            ctx.lineTo(1.2*e.offsetX, 1.3*e.offsetY);
            ctx.lineTo(1.16*e.offsetX-0.5*lastX, 1.5*e.offsetY-0.7*lastY);
            setTimeout(()=>{ctx.stroke()},5);
            ctx.closePath();

            break;
        
        case 5: //rectangle
            if(!isDrawing)  return; //沒有允許繪製即退出
            //console.log("rec");
            /*繪製路線 Setting*/
            if(isRainbow==1){
                ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色

                hue++;  //色相環度數更新
                if(hue>=360)
                {
                    hue=0;
                }
            }
            else if(isRainbow==-1){
                ctx.strokeStyle=nowColor;
            }
            canvasPic.src = cPushArray[cStep];            
            canvasPic.onload = function () { 
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic, 0, 0); 
            }
            var rec_width=e.offsetX-lastX;
            var rec_height=e.offsetY-lastY;
            ctx.beginPath();    //開始路徑 or Reset
            ctx.moveTo(lastX, lastY);   //設定起點
            ctx.rect(lastX, lastY, rec_width, rec_height);
            setTimeout(()=>{ctx.stroke()},5);
            ctx.closePath();

            break;

        default:
            break;
    }
}

/*調色*/
function setTextColor(picker) {
    nowColor = '#' + picker.toString();
}

/*調筆刷大小*/
function Size(s){
    if(s==1 && ctx.lineWidth<500){
        ctx.lineWidth=ctx.lineWidth+5;
        s=0;
    }
    else if(s==-1 && ctx.lineWidth>0){
        ctx.lineWidth=ctx.lineWidth-5;
        s=0;
    }
    else{
        ctx.lineWidth=ctx.lineWidth;
    }
}

/*調字體大小*/
function textSize(t){
    console.log(nowTextsize);
    if(t==1 && nowTextsize<500){
        nowTextsize=nowTextsize+5;
        t=0;
    }
    else if(t==-1 && nowTextsize>0){
        nowTextsize=nowTextsize-5;
        t=0;
    }
    else{
        nowTextsize=nowTextsize;
    }
}

/*打字*/
function Context(event){
    var text=document.createElement("input");
    text.style.position="absolute";
    text.style.top=`${lastY}px`;
    text.style.left=`${lastX}px`;
    var text_area=document.getElementById("myCanvasBackground");
    text_area.appendChild(text);
    var fe=document.getElementById("FontEffect");
    var ft=document.getElementById("FontType");
    ctx.font=`${fe.options[fe.selectedIndex].value} ${nowTextsize}px ${ft.options[ft.selectedIndex].value}`;
    ctx.fillStyle=nowColor;
    //text.type="text";
    //text.placeholder="Type here";
    //text.maxLength="50";
    //text.style.border="1px dotted transparent";
    //text.style.borderRadius="7px";
    //text.style.background="transparent";
    text.style.color=nowColor;
    var rec = canvas.getBoundingClientRect();
    text.addEventListener("keydown", function(event){
        if(event.keyCode==13){
            isText=false;
            ctx.fillText(`${text.value}`, lastX, lastY);
            //ctx.fillText(`${text.value}`, event.offsetX, event.offsetY);
            //ctx.fillText(text.value, event.pageX-rec.left, event.pageY-rec.top);
            //ctx.fillText(text.value, canvas.width/2, canvas.height/2);
            ctx.stroke();
            //Store();
            text.remove();
        }
    });
}

/*字體樣式*/
function fontEffect(fE){
    //console.log(fE);
    FontEffect=fE;
}
function fontType(fT){
    FontType=fT;
}

/*存每個動作的畫布、undo、redo*/
function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById('myCanvas').toDataURL());
    /*document.title = cStep + ":" + cPushArray.length;*/
}
function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        /*document.title = cStep + ":" + cPushArray.length;*/
    }
}
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        /*document.title = cStep + ":" + cPushArray.length;*/
    }
}

/*上傳*/
function upload(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,0,0);
            cPush();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}

/*下載*/
function download(){
    var link = document.createElement('a');
    link.download = "myCanvas.png";
    link.href = canvas.toDataURL();
    link.click();
  }

/*換游標、預設*/
function ToolList(i){
    switch(i){
        case 0:
            nowTool=0;
            canvas.style.cursor="url('pencil.png'), default";
            break;
        
        case 1:
        //console.log(nowTool);
            nowTool=1;
            canvas.style.cursor="url('eraser.png'), default";
            break;

        case 2:
            nowTool=2;
            isText=true;
            break;
        
        case 3:
            nowTool=3;
            break;

        case 4:
            nowTool=4;
            break;

        case 5:
            nowTool=5;
            break;
        
        case 6:
            isRainbow=-isRainbow;
            //console.log(isRainbow);
            canvas.style.cursor="url('rainbow.png'), default";
            break;

        case 7:
            isDirection=-isDirection;
            canvas.style.cursor="crosshair";
            break;

        default:
            break;
    }
}

/*彩色功能函數撰寫2*/
/*---------- Rainbow 功能 ----------*/
/*let hue=0;  //色相環度數從0開始

function rainbow()
{
    ctx.strokeStyle=`hsl(${hue}, 100%, 50%)`;   //透過context的strokeStyle設定筆畫顏色

    hue++;  //色相環度數更新
    if(hue>=360)
    {
        hue=0;
    }
}*/

/*動態筆觸粗細功能函數撰寫3*/
/*---------- 動態筆觸 功能 ----------*/
/** 
 * for 筆觸大小。 
 *
 * true 為 "細到粗"
 * false 為"粗到細"
 */
/*let direction=true;

function telescopicWidth()
{
    //如果>=100或<=1 則筆觸大小反向動作
    if(ctx.lineWidth>=100 || ctx.lineWidth<=1)
    {
        direction=!direction;
    }

    //筆觸粗細實作
    if(direction)
    {
        ctx.lineWidth++;
    }
    else
    {
        ctx.lineWidth--;
    }
}*/